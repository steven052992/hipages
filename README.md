# Hipages Test App (Android)

I have used the MVVM design pattern as my base architecture for the sample application because;

* Better separation of concerns by adding another layer, the viewmodel. On top of that it also does not ties us to the lifecycle of the view.
* Improved testability by migrating the data manipulation to the view model
* This way it can survivce configuration changes and avoid memory leaks

MVVM is also part of the Android Architecture Components, which help us build a
Robust, Testable, and Maintainable application.

