package au.com.hipagesgroup.testapp.ui

import au.com.hipagesgroup.testapp.data.Job

interface CloseJobListener {

    fun onCloseJob(job: Job)

}