package au.com.hipagesgroup.testapp.ui

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import au.com.hipagesgroup.testapp.R
import au.com.hipagesgroup.testapp.data.Job
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), CloseJobListener {

    private lateinit var jobListPagerAdapter : JobListPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.hide()
        jobListPagerAdapter = JobListPagerAdapter(supportFragmentManager, this)
        joblist_pager.adapter = jobListPagerAdapter

    }

    override fun onCloseJob(job: Job) {
        val jobListPagerItem = supportFragmentManager.fragments[1]

        for (fragment: Fragment in supportFragmentManager.fragments) {
            if ((jobListPagerItem as JobListFragment).fragmentType == JobListFragment.FragmentType.CLOSED) {
                jobListPagerItem.addJob(job)
                return
            }
        }

    }

}

class JobListPagerAdapter (fm: FragmentManager, private val mainActivity: MainActivity) : FragmentStatePagerAdapter(fm) {

    private val pages = arrayOf("Open Jobs", "Closed Jobs")

    override fun getCount(): Int  = pages.size

    override fun getItem(i: Int): Fragment {
        when(i) {
            0 -> {
                val jobListFragment = JobListFragment()
                jobListFragment.fragmentType = JobListFragment.FragmentType.OPEN
                jobListFragment.closeJobListener = mainActivity
                return jobListFragment
            }
            1 -> {
                val jobListFragment = JobListFragment()
                jobListFragment.fragmentType = JobListFragment.FragmentType.CLOSED
                return jobListFragment
            }
        }
        return JobListFragment()
    }

    override fun getPageTitle(position: Int): CharSequence {
        return pages[position]
    }

}