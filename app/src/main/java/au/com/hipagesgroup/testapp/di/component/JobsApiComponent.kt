package au.com.hipagesgroup.testapp.di.component

import au.com.hipagesgroup.testapp.di.module.JobApiModule
import au.com.hipagesgroup.testapp.di.module.NetworkModule
import au.com.hipagesgroup.testapp.network.JobsApi
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class, JobApiModule::class])
interface JobsApiComponent {

    fun getJobsApi(): JobsApi

}