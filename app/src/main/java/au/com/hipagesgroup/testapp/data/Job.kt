package au.com.hipagesgroup.testapp.data

data class JobBase(
    val jobs: List<Job>
)

data class Job(
    val jobId: Long = 0,
    val category: String?,
    val postedDate: String?,
    val status: String?,
    val connectedBusinesses: List<ConnectedBusiness>?,
    val isClosed: Boolean
)

data class ConnectedBusiness(
    val jobId: Long = 0,
    val businessId: Long = 0,
    val thumbnail: String?,
    val isHired: Boolean
)
