package au.com.hipagesgroup.testapp.ui

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import au.com.hipagesgroup.testapp.LOG_TAG
import au.com.hipagesgroup.testapp.data.Job
import au.com.hipagesgroup.testapp.di.component.DaggerJobsApiComponent
import au.com.hipagesgroup.testapp.di.component.JobsApiComponent
import au.com.hipagesgroup.testapp.di.module.JobApiModule
import au.com.hipagesgroup.testapp.di.module.NetworkModule
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class JobListViewModel: ViewModel() {

    private val mDisposables = CompositeDisposable()

    private var mJobsApiComponent: JobsApiComponent = DaggerJobsApiComponent
        .builder()
        .networkModule(NetworkModule)
        .jobApiModule(JobApiModule)
        .build()

    var mJobLiveData: MutableLiveData<List<Job>>? = MutableLiveData()

    override fun onCleared() {
        super.onCleared()
        mDisposables.dispose()
    }

    fun getJobs() {
        mDisposables.add(mJobsApiComponent.getJobsApi().getJobs()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({jobsBase ->
                mJobLiveData?.value = jobsBase.jobs
            })
            { throwable ->
                Log.e(LOG_TAG, throwable.localizedMessage)
            })
    }

    fun saveJob(job: Job) {

    }
}