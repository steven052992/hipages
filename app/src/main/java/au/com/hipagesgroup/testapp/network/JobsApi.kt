package au.com.hipagesgroup.testapp.network

import au.com.hipagesgroup.testapp.data.JobBase
import io.reactivex.Single
import retrofit2.http.GET


interface JobsApi {

    @GET("jobs.json")
    fun getJobs(): Single<JobBase>

}