package au.com.hipagesgroup.testapp.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import au.com.hipagesgroup.testapp.ui.JobListViewModel


@Suppress("UNCHECKED_CAST")
class ViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(JobListViewModel::class.java)) {
            return JobListViewModel() as T
        }
        throw IllegalArgumentException("Unknown class")
    }

}