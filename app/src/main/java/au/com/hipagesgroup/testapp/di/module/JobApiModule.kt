package au.com.hipagesgroup.testapp.di.module

import au.com.hipagesgroup.testapp.network.JobsApi
import dagger.Module
import dagger.Provides
import dagger.Reusable
import retrofit2.Retrofit

@Module
object JobApiModule {

    @Provides
    @Reusable
    internal fun provideJobsApi(retrofit: Retrofit): JobsApi {
        return retrofit.create(JobsApi::class.java)
    }

}