package au.com.hipagesgroup.testapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import au.com.hipagesgroup.testapp.R
import au.com.hipagesgroup.testapp.base.ViewModelFactory
import au.com.hipagesgroup.testapp.data.Job
import au.com.hipagesgroup.testapp.ui.adapter.JobAdapter
import kotlinx.android.synthetic.main.fragment_joblist.*

class JobListFragment: Fragment(), CloseJobListener {

    enum class FragmentType {
        OPEN, CLOSED
    }

    var closeJobListener: CloseJobListener? = null
    var fragmentType: FragmentType? = null

    private lateinit var mViewModel: JobListViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_joblist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mViewModel = ViewModelProviders.of(this, ViewModelFactory()).get(JobListViewModel::class.java)

        mViewModel.getJobs()

        activity?.let {
            val adapter = JobAdapter(it, this)
            rv_joblist.adapter = adapter
            rv_joblist.layoutManager = LinearLayoutManager(it)
            fragmentType?.let { fragType ->
                if (fragType == FragmentType.OPEN) {
                    mViewModel.mJobLiveData?.observe(this, Observer<List<Job>> { jobs ->
                    adapter.mJobs = jobs.toMutableList()
                    adapter.notifyDataSetChanged()
                    })
                } else {

                }
            }
        }
    }

    override fun onCloseJob(job: Job) {
        closeJobListener?.onCloseJob(job)
    }

    fun addJob(job: Job) {
        val adapter = rv_joblist.adapter
        (adapter as JobAdapter).mJobs.add(job)
        adapter.notifyDataSetChanged()
    }

}