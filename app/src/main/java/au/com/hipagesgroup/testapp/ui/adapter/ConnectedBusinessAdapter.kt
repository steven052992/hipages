package au.com.hipagesgroup.testapp.ui.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import au.com.hipagesgroup.testapp.R
import au.com.hipagesgroup.testapp.data.ConnectedBusiness
import au.com.hipagesgroup.testapp.util.inflate
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_connected_business.view.*


class ConnectedBusinessAdapter(private val mContext: Context) :
    RecyclerView.Adapter<ConnectedBusinessAdapter.ConnectedBusinessViewHolder>() {

    var mConnectedBusiness: List<ConnectedBusiness> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ConnectedBusinessViewHolder {
        return ConnectedBusinessViewHolder(parent, mContext)
    }

    override fun onBindViewHolder(viewHolder: ConnectedBusinessViewHolder, position: Int) {
        val item = mConnectedBusiness[position]
        viewHolder.bind(item)
    }

    override fun getItemCount(): Int {
        return mConnectedBusiness.size
    }

    class ConnectedBusinessViewHolder(parent: ViewGroup, val mContext: Context) : RecyclerView.ViewHolder(
        parent.inflate(R.layout.item_connected_business)
    ) {
        fun bind(connectedBusiness: ConnectedBusiness?) = connectedBusiness?.let {
            if (it.isHired) {
                itemView.tv_hired.visibility = View.VISIBLE
            } else {
                itemView.tv_hired.visibility = View.GONE
            }
            Glide
                .with(mContext)
                .load(connectedBusiness.thumbnail)
                .centerCrop()
                .into(itemView.iv_thumbnail);
        }
    }
}