package au.com.hipagesgroup.testapp.ui.adapter

import android.content.Context
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import au.com.hipagesgroup.testapp.R
import au.com.hipagesgroup.testapp.data.Job
import au.com.hipagesgroup.testapp.ui.CloseJobListener
import au.com.hipagesgroup.testapp.util.inflate
import kotlinx.android.synthetic.main.item_job.view.*


class JobAdapter(private val mContext: Context, val closeJobListener: CloseJobListener):
    RecyclerView.Adapter<JobAdapter.JobViewHolder>() {

    var mJobs: MutableList<Job> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): JobViewHolder {
        return JobViewHolder(parent, mContext, closeJobListener)
    }

    override fun onBindViewHolder(viewHolder: JobViewHolder, position: Int) {
        val item = mJobs[position]
        viewHolder.bind(item)

        val popup = PopupMenu(mContext, viewHolder.itemView.tv_options)
        popup.inflate(R.menu.job_menu)

        popup.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.close -> {
                    closeJobListener.onCloseJob(item)
                    mJobs.removeAt(position)
                    notifyDataSetChanged()
                    true
                }
                else -> false
            }
        }

        viewHolder.itemView.tv_options.setOnClickListener {
            popup.show()
        }

    }

    override fun getItemCount(): Int {
        return mJobs.size
    }

    class JobViewHolder(parent: ViewGroup, val mContext: Context, val closeJobListener: CloseJobListener): RecyclerView.ViewHolder(
        parent.inflate(R.layout.item_job)
    ) {

        fun bind(job: Job?) = job?.let {
            itemView.tv_job_category.text = it.category
            itemView.tv_job_date.text = "Posted: ${it.postedDate}"
            itemView.tv_job_status.text = it.status

            if (it.connectedBusinesses.isNullOrEmpty()) {
                itemView.tv_businesses_lbl.text = mContext.getString(R.string.connecting)
            } else {
                itemView.tv_businesses_lbl.text = String.format(
                    mContext.getString(R.string.hired_buss),
                    it.connectedBusinesses.size
                )
            }

            job.connectedBusinesses?.let { connectedBuss ->
                val adapter = ConnectedBusinessAdapter(mContext)
                itemView.rv_businesses.adapter = adapter
                itemView.rv_businesses.layoutManager = GridLayoutManager(mContext, 4)
                adapter.mConnectedBusiness = connectedBuss
                adapter.notifyDataSetChanged()
            }
        }
    }
}
