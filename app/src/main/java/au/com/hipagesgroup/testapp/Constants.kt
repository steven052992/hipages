package au.com.hipagesgroup.testapp

const val BASE_URL: String = "https://s3-ap-southeast-2.amazonaws.com/hipgrp-assets/tech-test/"
const val LOG_TAG: String = "HiPages Test App"